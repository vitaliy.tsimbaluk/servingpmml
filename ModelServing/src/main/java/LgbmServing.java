import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;
import com.beust.jcommander.ParameterException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import org.dmg.pmml.FieldName;
import org.jpmml.evaluator.Evaluator;
import org.jpmml.evaluator.FieldValue;
import org.jpmml.evaluator.InputField;
import org.jpmml.evaluator.LoadingModelEvaluatorBuilder;
import org.jpmml.evaluator.ProbabilityDistribution;
import org.jpmml.evaluator.ServiceLoadingModelEvaluatorBuilder;
import org.jpmml.transpiler.FileTranspiler;
import org.jpmml.transpiler.TranspilerTransformer;
import org.xml.sax.SAXException;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.URL;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class LgbmServing {
    public static final int RANDOM_INVOCATION = 1_000_000;
    @Parameter(
            names = {"--lgbm-test"},
            description = "LightGBM test csv file",
            required = true
    )
    private File testCsv = null;

    @Parameter(
            names = {"--lgbm-predictions"},
            description = "LightGBM prediction results csv file",
            required = true
    )
    private File resultsCsv = null;

    @Parameter(
            names = {"--lgbm-model"},
            description = "LightGBM jar file",
            required = true
    )
    private File lgbmModel = null;

    public static List<Map<String, String>> read(File file) throws IOException {
        var response = new LinkedList<Map<String, String>>();
        var mapper = new CsvMapper();
        var schema = CsvSchema.emptySchema().withLineSeparator(System.lineSeparator()).withHeader();
        var iterator = mapper.readerFor(Map.class)
                .with(schema)
                .<Map<String, String>>readValues(file);
        while (iterator.hasNext()) {
            response.add(iterator.next());
        }
        return response;
    }

    public static void write(File file, List<Map<String, String>> data) throws IOException {
        Writer writer = new FileWriter(file, false);
        CsvSchema.Builder schemaBuilder = CsvSchema.builder();
        for (String col : data.get(0).keySet()) {
            schemaBuilder.addColumn(col);
        }
        var schema = schemaBuilder.build().withLineSeparator(System.lineSeparator()).withHeader();
        CsvMapper mapper = new CsvMapper();
        mapper.writer(schema).writeValues(writer).writeAll(data);
        writer.flush();
    }

    public static void main(String... argv) {
        var app = new LgbmServing();
        var commander = new JCommander(app);
        try {
            commander.parse(argv);
            app.run();
        } catch (ParameterException | IOException | JAXBException | SAXException e) {
            commander.usage();
            e.printStackTrace();
        }
    }

    private Evaluator loadModel() throws JAXBException, IOException, SAXException {
        if (lgbmModel.getName().endsWith(".pmml")) {
            // load model from pmml file and transpile to jar
            return new LoadingModelEvaluatorBuilder()
                    .load(lgbmModel)
                    .transform(new TranspilerTransformer(new FileTranspiler(null,
                            new File(lgbmModel.getName().concat(".jar")))))
                    .build();
        } else {
            // load model from jar file
            URL pmmlJarURL = (lgbmModel.toURI()).toURL();

            return new ServiceLoadingModelEvaluatorBuilder()
                    .loadService(pmmlJarURL)
                    .build();
        }
    }

    private void run() throws IOException, JAXBException, SAXException {
        System.out.println("reading dataset:");
        // load data and convert to required format for inference
        var data = read(testCsv);
        var evaluator = loadModel();
        var inputFields = evaluator.getInputFields();
        var field = evaluator.getTargetFields().get(0);
        var random = new Random();

        // first calculate predictions and save to file to make sure they are correct
        for (var row : data) {
            var input = populateInput(inputFields, row);
            // execute prediction
            var result = ((ProbabilityDistribution<?>) evaluator.evaluate(input).get(field.getFieldName()))
                    .getResult();
            // emulate saving prediction result somewhere
            row.put("prediction", String.valueOf(result));
        }
        write(resultsCsv, data);

        // do inference and calculate overall time over big amount of random rows
        System.out.println("start evaluation:");
        long start = System.nanoTime();
        for (int i = 0; i < RANDOM_INVOCATION; i++) {
            // select random row
            var row = data.get(random.nextInt(data.size()));
            var input = populateInput(inputFields, row);
            // execute prediction
            var result = ((ProbabilityDistribution<?>) evaluator.evaluate(input).get(field.getFieldName()))
                    .getResult();
            // emulate saving prediction result somewhere
            row.put("prediction", String.valueOf(result));
        }
        long end = System.nanoTime();

        // print average time
        double duration = (double) (end - start) / 1_000;
        double average = duration / RANDOM_INVOCATION;
        System.out.printf("size: %d, testing %d random invocation: duration: %f µs, average: %f µs %n", data.size(),
                RANDOM_INVOCATION, duration, average);

    }

    private Map<FieldName, FieldValue> populateInput(List<InputField> inputFields, Map<String, String> row) {
        Map<FieldName, FieldValue> input = new LinkedHashMap<>();
        for (InputField inputField : inputFields) {
            FieldName inputName = inputField.getName();
            Object rawValue = row.get(inputName.getValue());
            FieldValue inputValue = inputField.prepare(rawValue);
            input.put(inputName, inputValue);
        }
        return input;
    }
}
