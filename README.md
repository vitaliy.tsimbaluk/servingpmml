# ServingPMML

ThinkDataScience @ Nix 

## About this project

Project contains jupiter notebook to train a lgbm wine classsifier (with export as pmml model) and Java application to import a model and serve it

Note that the project uses https://github.com/jpmml/jpmml-lightgbm/ as model converter and the corresponding Java libs to load serve the model. Please clone that repo separately.

## How to use

1. Train the model and save it
2. If youi save it as via lgmb booster the model needs to be converted to pmml as follow
   java -jar jpmml-lightgbm/pmml-lightgbm-example/target/pmml-lightgbm-example-executable-1.4-SNAPSHOT.jar --lgbm-input WineClassifier/wine_classifier_1.txt --pmml-output WineClassifier/wine_classifier_1.pmml
3. Run java app to serve the model with the follow params:
   --lgbm-test WineClassifier/wine_test.csv --lgbm-predictions WineClassifier/wine_predictions.csv --lgbm-model WineClassifier/wine_classifier_1.pmml

